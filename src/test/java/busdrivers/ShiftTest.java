package busdrivers;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class ShiftTest {

    @Test
    void should_return_one_when_the_same_stop_is_common_to_the_two_routes() {
        Driver driver = new Driver(new Route(getStops(1)));
        Driver driver2 = new Driver(new Route(getStops(1)));
        Optional<NumberOfStops> numberOfRoutes = compute(driver, driver2);

        assertThat(numberOfRoutes).contains(new NumberOfStops(1));
    }

    @Test
    void should_return_never_when_there_is_no_common_stops_on_the_two_routes() {
        Driver driver = new Driver(new Route(getStops(1)));
        Driver driver2 = new Driver(new Route(getStops(2)));
        Optional<NumberOfStops> compute = compute(driver, driver2);

        assertThat(compute).isEmpty();
    }

    private Optional<NumberOfStops> compute(Driver driver, Driver driver2) {
        Shift shift = new Shift(List.of(driver, driver2));
        return shift.countOfStopsToShareAllGossips();
    }

    @Test
    void should_return_one_when_there_is_a_common_stop_at_the_second_iteration() {
        Driver driver = new Driver(new Route(getStops(1, 2)));
        Driver driver2 = new Driver(new Route(getStops(3, 2)));
        Optional<NumberOfStops> compute = compute(driver, driver2);

        assertThat(compute).contains(new NumberOfStops(2));
    }

    @Test
    void should_return_never_when_there_is_no_common_stop() {
        Driver driver = new Driver(new Route(getStops(1, 2)));
        Driver driver2 = new Driver(new Route(getStops(3, 4)));
        Optional<NumberOfStops> compute = compute(driver, driver2);

        assertThat(compute).isEmpty();
    }

    @Test
    void should_return_one_when_the_common_stop_is_at_the_first() {
        Driver driver = new Driver(new Route(getStops(1, 2)));
        Driver driver2 = new Driver(new Route(getStops(1, 4)));
        Optional<NumberOfStops> compute = compute(driver, driver2);

        assertThat(compute).contains(new NumberOfStops(1));
    }

    @Test
    void should_return_one_when_the_common_stop_is_at_the_first_for_three_stops() {
        Driver driver = new Driver(new Route(getStops(1, 2, 3)));
        Driver driver2 = new Driver(new Route(getStops(1, 4, 5)));
        Optional<NumberOfStops> compute = compute(driver, driver2);

        assertThat(compute).contains(new NumberOfStops(1));
    }

    @Test
    void should_return_three_when_the_common_stop_is_the_third() {
        Driver driver = new Driver(new Route(getStops(4, 2, 3)));
        Driver driver2 = new Driver(new Route(getStops(1, 4, 3)));
        Optional<NumberOfStops> compute = compute(driver, driver2);

        assertThat(compute).contains(new NumberOfStops(3));
    }

    @Test
    void should_return_three_when_the_common_stop_is_the_third_on_different_itineraries_sizes() {
        Driver driver = new Driver(new Route(getStops(4, 2, 3)));
        Driver driver2 = new Driver(new Route(getStops(1, 4, 3, 5)));
        Optional<NumberOfStops> compute = compute(driver, driver2);

        assertThat(compute).contains(new NumberOfStops(3));
    }

    @Test
    void should_return_three_when_the_common_stop_is_the_third_on_different_itineraries() {
        Driver driver = new Driver(new Route(getStops(3, 2, 4)));
        Driver driver2 = new Driver(new Route(getStops(1, 4, 3, 5)));
        Optional<NumberOfStops> compute = compute(driver2, driver);

        assertThat(compute).contains(new NumberOfStops(3));
    }

    private List<Integer> getStops(Integer... i) {
        return List.of(i);
    }


}