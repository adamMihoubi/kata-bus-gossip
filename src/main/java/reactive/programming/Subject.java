package reactive.programming;

import java.util.ArrayList;
import java.util.List;

public class Subject {
    private final List<Observer> observers = new ArrayList<>();

    public void register(Observer observer) {
        observers.add(observer);
    }

    public void notifyObservers(Color color) {
        observers.forEach(observer -> observer.notify(color));
    }

}
