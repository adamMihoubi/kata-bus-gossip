package busdrivers;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.IntStream;

public class Route {
    private final List<Integer> stops;

    public Route(List<Integer> stops) {
        this.stops = stops;
    }

    public Optional<NumberOfStops> commonStop(Route route) {
        return IntStream.range(0, route.stops.size())
                .filter(place -> stops.get(place).equals(route.stops.get(place)))
                .mapToObj(place -> new NumberOfStops(++place))
                .findFirst();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route1 = (Route) o;
        return Objects.equals(stops, route1.stops);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stops);
    }
}
