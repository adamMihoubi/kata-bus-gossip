package busdrivers;

import java.util.Objects;

public class NumberOfStops {
    public static final NumberOfStops NEVER = new NumberOfStops(-1);
    private final Integer value;

    public NumberOfStops(Integer value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NumberOfStops numberOfStops = (NumberOfStops) o;
        return Objects.equals(value, numberOfStops.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "Count{" +
                "value=" + value +
                '}';
    }
}
