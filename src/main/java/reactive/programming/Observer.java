package reactive.programming;

public interface Observer {
    void notify(Color color);
}
