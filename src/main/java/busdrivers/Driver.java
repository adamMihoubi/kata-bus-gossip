package busdrivers;

import java.util.Optional;

public class Driver {
    private final Route route;

    public Driver(Route route) {
        this.route = route;
    }

    public boolean haveSameRouteAs(Driver driver) {
        return route.equals(driver.route);
    }

    public Optional<NumberOfStops> commonStop(Driver driver) {
        return route.commonStop(driver.route);
    }
}
