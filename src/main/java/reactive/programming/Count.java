package reactive.programming;

public class Count {
    private Integer count = 0;

    public void increment() {
        count++;
    }

    public Integer getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "Count{" +
                "count=" + count +
                '}';
    }
}
