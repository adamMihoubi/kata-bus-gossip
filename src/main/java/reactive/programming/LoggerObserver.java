package reactive.programming;

public class LoggerObserver implements Observer {

    @Override
    public void notify(Color color) {
        System.out.println("couleur : " + color.name());
    }
}
