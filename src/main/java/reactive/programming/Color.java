package reactive.programming;

public enum Color {
    BLUE,
    RED,
    BLACK,
    WHITE
}
