package busdrivers;

import java.util.List;
import java.util.Optional;

public class Shift {
    private final List<Driver> drivers;

    public Shift(List<Driver> drivers) {
        this.drivers = drivers;
    }

    public Optional<NumberOfStops> countOfStopsToShareAllGossips() {
        return drivers.get(0).commonStop(drivers.get(1));
    }
}
