package reactive.programming;

import java.util.HashMap;
import java.util.Map;

public class ColorObserver implements Observer {
    private final Map<Color, Count> counts = new HashMap<>();

    @Override
    public void notify(Color color) {
        counts.putIfAbsent(color, new Count());
        counts.get(color).increment();
    }

    public Integer numberOf(Color color) {
        return counts.get(color).getCount();
    }
}
