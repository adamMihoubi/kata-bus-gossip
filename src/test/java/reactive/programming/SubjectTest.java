package reactive.programming;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class SubjectTest {

    @Test
    void should_return_number_of_colors() {
        //given
        List<Color> colors = List.of(Color.BLUE, Color.RED, Color.BLUE, Color.WHITE, Color.WHITE, Color.RED, Color.BLACK);
        Subject subject = new Subject();
        ColorObserver colorObserver = new ColorObserver();
        subject.register(colorObserver);
        subject.register(new LoggerObserver());

        //when
        colors.forEach(subject::notifyObservers);

        //then
        assertThat(colorObserver.numberOf(Color.BLUE)).isEqualTo(2);
        assertThat(colorObserver.numberOf(Color.RED)).isEqualTo(2);
        assertThat(colorObserver.numberOf(Color.WHITE)).isEqualTo(2);
        assertThat(colorObserver.numberOf(Color.BLACK)).isEqualTo(1);
    }
}